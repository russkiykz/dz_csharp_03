﻿using System;

namespace DZ_03_04._12._2020
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1.
            Console.Write("Введите символы (для выхода введите знак точки): ");
            char symbol = ' ';
            int numberOfSpaces = 0;
            while (symbol != '.')
            {
                symbol = Console.ReadKey().KeyChar;
                if (symbol == ' ')
                {
                    numberOfSpaces++;
                }
            }
            Console.WriteLine($"\nКоличество пробелов составляет: {numberOfSpaces}");
            Console.WriteLine("-----------");

            // 2.
            Console.Write("Введите номер билета(6 значное значение): ");
            int luckyTicketNumber = int.Parse(Console.ReadLine());
            int firstHalf = 0, secondHalf = 0;
            if (luckyTicketNumber > 99999 && luckyTicketNumber < 1000000)
            {
                for (int i = 0; i < 3; i++)
                {
                    firstHalf += luckyTicketNumber % 10;
                    luckyTicketNumber /= 10;
                }
                for (int i = 0; i < 3; i++)
                {
                    secondHalf += luckyTicketNumber % 10;
                    luckyTicketNumber /= 10;
                }
                if (firstHalf == secondHalf)
                {
                    Console.WriteLine("\nСчастливый билет");
                }
                else
                {
                    Console.WriteLine("\nОбычный билет");
                }
            }
            else
                Console.WriteLine("\nНеверное значение!");
            Console.WriteLine("-----------");

            // 3.
            int numberCodeASCII = 0;
            char symbolASCII;
            Console.Write("Введите символы (для выхода введите знак точки): ");
            while (true)
            {
                do
                {
                    numberCodeASCII = Console.Read();
                    symbolASCII = (char)numberCodeASCII;
                } while (symbolASCII == '\n' || symbolASCII == '\r');

                if (numberCodeASCII >= 65 && numberCodeASCII <= 90)
                {
                    numberCodeASCII += 32;
                    symbolASCII = (char)numberCodeASCII;
                    Console.Write(symbolASCII);
                }
                else if (numberCodeASCII >= 97 && numberCodeASCII <= 122)
                {
                    numberCodeASCII -= 32;
                    symbolASCII = (char)numberCodeASCII;
                    Console.Write(symbolASCII);
                }
                else if (numberCodeASCII == 46)
                {
                    break;
                }
                else
                {
                    symbolASCII = (char)numberCodeASCII;
                    Console.Write(symbolASCII);
                }
            }
            Console.ReadLine();
            Console.WriteLine("\n-----------");


            // 4.
            Console.Write("Введите значение А: ");
            int valueA = int.Parse(Console.ReadLine());
            Console.Write("Введите значение B: ");
            int valueB = int.Parse(Console.ReadLine());
            while (valueA <= valueB)
            {
                for (int i = 0; i < valueA; i++)
                {
                    Console.Write(valueA);
                }
                Console.WriteLine();
                valueA++;
            }
            Console.WriteLine("-----------");

            // 5.
            Console.Write("Введите значение N: ");
            int valueN = int.Parse(Console.ReadLine());
            Console.Write("Результат: ");
            while (valueN >= 10)
            {
                Console.Write(valueN % 10);
                valueN /= 10;
            }
            Console.Write(valueN);

        }
    }
}
